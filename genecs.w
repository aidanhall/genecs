@* An Introduction To The General Entity Component System.
An Entity Component System is an approach to structuring programs which
makes better use of the performance characteristics of modern computers.
These include fast processors and caches, pipelining and slow main memory.
I will attempt to use a data-oriented approach.

The core idea is to separate data and code.
Data is stored in a set of data structures called
{\bf components}.
These are generally structures that represent the smallest meaningful
collection of state information
there can be.
An
{\bf entity}
is a collection of certain components.
Most of the non-organisational code should be found in
{\bf system}.
These run continuously, and I will implement them as functions that are called each frame.
Systems will operate on all entities that have at least a certain set of components.
@c
#include "genecs.h"
int main(int argc, char* argv[])
{
	printf("hello world.\n");
}
@* Header.
There need to be header files that can be included by users of the library.
@(genecs.h@>=	@q)@>
#pragma once
#include <stdio.h>
#include <stdint.h>
