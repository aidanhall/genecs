# OPTIONS
CC = clang
CFLAGS = -Wall -g
# DIRS
BASE = $(CURDIR)

# WEB
WEBSOURCE = $(wildcard *.w)

# CODE
CSOURCE = $(WEBSOURCE:.w=.c)
OFILES = $(CSOURCE:.c=.o)
HFILES = $(CSOURCE:.c=.h)
	

# DOCUMENT
TEXSOURCE = $(WEBSOURCE:.w=.tex)
DOCUMENT = $(TEXSOURCE:.tex=.pdf)

all: $(OFILES) $(DOCUMENT)

force: clean all

%.pdf: %.tex
	pdftex $<

view: $(DOCUMENT)
	echo viewing $<
	zathura --fork $<

.SECONDARY: $(CSOURCE)

clean:
	$(RM) $(OFILES) $(HFILES) $(DOCUMENT) *.idx *.scn *.toc *.log
